# README #

### How to run it? ###
Run the following commands

1.  ```npm install```
2. ```npm run build```
3. ```npm run serve```
and the application should run on the port `4200`

### Design/Architecture decisions, Libraries used and why? ###
Project contains the following libraries/dependencies

  1. React - `UI library that is helping to render data in an component driven architecture`
  2. React-router - `For easy routing solution`
  3. Babel - `To transpile es6 and up to es5`
  4. Redux - `For state management, may be an overkill for simple application like this but it does show the emphasis on functional programming aspect of application development`
  5. Reselect - `Memoization library to minimize redux as good practise`
  6. Webpack - `Module bundler for the app`
  7. Scss - `Css preprocessor for following BEM and SMACSS principle, propably an overkill for this app`
  8. Eslint/Prettier - `Code linter and Formatter`
  9. idb-keyval - `Simpler Promised based library for Using IndexDB => Using IndexDb for data caching istead of Localstorage`
  10. rmwc - `An material design wrapper for react for pre styled component`

> Owner: esakkikrishnan2410@gmail.com
