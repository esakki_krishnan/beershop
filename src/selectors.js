import { createSelector } from 'reselect';
import { uniqBy, map, startsWith, orderBy } from 'lodash';

export const getMyBeersId = state => state.cart.beerIds;
export const getAllBeers = state => state.beers.data;
export const getMyBeers = createSelector(
  getAllBeers,
  getMyBeersId,
  (allBeers, myBeers) => allBeers.filter(e => myBeers.indexOf(e.id) >= 0),
);
export const getBeerStyles = createSelector(getAllBeers, allBeers =>
  uniqBy(allBeers, elm => elm.style).map((e, index) => ({
    value: index,
    label: e.style,
  })),
);

export const getBeerSortBy = state => state.beers.sortBy;
export const getBeerFilterById = state => state.beers.filterBy;
export const getBeerFilterBy = createSelector(
  getBeerStyles,
  getBeerFilterById,
  (allStyle, filterIndex) => (filterIndex ? allStyle[filterIndex] : null),
);
export const getBeerSearchText = state => state.beers.searchText;

export const getFilteredBeers = createSelector(
  getAllBeers,
  getBeerSortBy,
  getBeerFilterBy,
  getBeerSearchText,
  (allBeers, sortBy, filterBy, searchText) => {
    const filtered = filterBy
      ? allBeers.filter(beer => beer.style === filterBy.label)
      : allBeers;
    const searched = searchText
      ? filtered.filter(fbeer => startsWith(fbeer.name, searchText))
      : filtered;

    const sortByNum = parseInt(sortBy, 10);
    const sortText = sortByNum === 0 || sortByNum === 1 ? sortByNum : null;
    return sortText !== null
      ? orderBy(
          searched,
          [item => parseFloat(item.abv)],
          [sortText === 1 ? 'asc' : 'desc'],
        )
      : searched;
  },
);
