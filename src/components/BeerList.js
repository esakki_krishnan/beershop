import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'rmwc';
import BeerInfo from './BeerInfo';

// const beerList = [
//   {
//     abv: '0.05',
//     ibu: '12',
//     id: 1436,
//     name: 'Pub Beer',
//     style: 'American Pale Lager',
//     ounces: 12,
//   },
//   {
//     abv: '0.11',
//     ibu: '',
//     id: 2265,
//     name: 'Devils Cup',
//     style: 'American Pale Ale (APA)',
//     ounces: 12,
//   },
//   {
//     abv: '0.07',
//     ibu: '',
//     id: 2264,
//     name: 'Rise of the Phoenix',
//     style: 'American IPA',
//     ounces: 12,
//   },
//   {
//     abv: '0.09',
//     ibu: '',
//     id: 2263,
//     name: 'Sinister',
//     style: 'American Double / Imperial IPA',
//     ounces: 12,
//   },
//   {
//     abv: '0.07',
//     ibu: '',
//     id: 2262,
//     name: 'Sex and Candy',
//     style: 'American IPA',
//     ounces: 12,
//   },
//   {
//     abv: '0.07',
//     ibu: '',
//     id: 2261,
//     name: 'Black Exodus',
//     style: 'Oatmeal Stout',
//     ounces: 12,
//   },
//   {
//     abv: '0.04',
//     ibu: '',
//     id: 2260,
//     name: 'Lake Street Express',
//     style: 'American Pale Ale (APA)',
//     ounces: 12,
//   },
//   {
//     abv: '0.06',
//     ibu: '',
//     id: 2259,
//     name: 'Foreman',
//     style: 'American Porter',
//     ounces: 12,
//   },
//   {
//     abv: '0.05',
//     ibu: '',
//     id: 2258,
//     name: 'Jade',
//     style: 'American Pale Ale (APA)',
//     ounces: 12,
//   },
//   {
//     abv: '0.08',
//     ibu: '',
//     id: 2131,
//     name: 'Cone Crusher',
//     style: 'American Double / Imperial IPA',
//     ounces: 12,
//   },
//   {
//     abv: '0.07',
//     ibu: '',
//     id: 2099,
//     name: 'Sophomoric Saison',
//     style: 'Saison / Farmhouse Ale',
//     ounces: 12,
//   },
//   {
//     abv: '0.07',
//     ibu: '',
//     id: 2098,
//     name: 'Regional Ring Of Fire',
//     style: 'Saison / FarmhouseAle',
//     ounces: 12,
//   },
// ];

const BeerList = ({ beerList, onAddtoCart }) => (
  <div className="bearlist">
    {beerList.map(beerElm => (
      <BeerInfo addCart={id => onAddtoCart(id)} key={beerElm.id} {...beerElm} />
    ))}
  </div>
);
BeerList.propTypes = {
  beerList: PropTypes.arrayOf(Object).isRequired,
  onAddtoCart: PropTypes.func.isRequired,
};
BeerList.defaultProps = {
  onLoadMore: () => {
    console.log('LOADMORE');
  },
};

export default BeerList;
