import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Toolbar,
  ToolbarRow,
  ToolbarSection,
  ToolbarTitle,
  ToolbarMenuIcon,
  ToolbarIcon,
} from 'rmwc/Toolbar';

const AppBar = ({ headerTitle, onMenuClick, menuIcon }) => (
  <Toolbar className="mdc-elevation--z6">
    <ToolbarRow>
      <ToolbarSection alignStart>
        <ToolbarMenuIcon onClick={onMenuClick} use={menuIcon} />
        <ToolbarTitle>{headerTitle}</ToolbarTitle>
      </ToolbarSection>
      <ToolbarSection alignEnd>
        <Link to="/cart">
          <ToolbarIcon use="shopping_cart" />
        </Link>
      </ToolbarSection>
    </ToolbarRow>
  </Toolbar>
);

AppBar.propTypes = {
  headerTitle: PropTypes.string.isRequired,
  onMenuClick: PropTypes.func.isRequired,
  menuIcon: PropTypes.string,
};
AppBar.defaultProps = {
  menuIcon: 'menu',
};

export default AppBar;
