import React from 'react';
import PropTypes from 'prop-types';
import { ButtonIcon, Icon } from 'rmwc';

const alcoIcon = [
  'filter_none',
  'filter_1',
  'filter_2',
  'filter_3',
  'filter_4',
  'filter_5',
  'filter_6',
  'filter_7',
  'filter_8',
  'filter_9',
  'filter_9_plus',
];

const bitterIcon = [
  'looks_one',
  'looks_two',
  'looks_3',
  'looks_4',
  'looks_5',
  'looks_6',
];

const BeerInfo = ({ id, abv, ibu, name, style, ounces, addCart }) => (
  <div className="beer">
    <div className="beer__title">
      <p>
        {name} {id}
      </p>
      <ButtonIcon use="add" onClick={() => addCart(id)} />
    </div>
    <div className="beer__info">
      <p>STYLE: {style}</p>
      <p>OUNCES: {ounces}</p>
    </div>
    <div className="beer__taste">
      <p>
        <span>ALCOHOL CONTENT: </span>
        <Icon strategy="ligature">
          {abv ? alcoIcon[Math.floor(parseFloat(abv) * 100) - 1] : 'error'}
        </Icon>
      </p>
      <p>
        <span>Bitterness: </span>
        <Icon strategy="ligature">
          {ibu ? bitterIcon[Math.ceil(parseInt(ibu, 10) / 20) - 1] : 'error'}
        </Icon>
      </p>
    </div>
  </div>
);
BeerInfo.propTypes = {
  id: PropTypes.number.isRequired,
  abv: PropTypes.string.isRequired,
  ibu: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  style: PropTypes.string.isRequired,
  ounces: PropTypes.number.isRequired,
  addCart: PropTypes.func.isRequired,
};

export default BeerInfo;
