import React from 'react';

const Spinner = () => (
  <div className="spinner spinner--bg spinner--rel spinner--overlay spinner--large" />
);

export default Spinner;
