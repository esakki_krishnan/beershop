import React from 'react';
import PropTypes from 'prop-types';

const StatusMessage = ({ StatusIcon, title, message }) => (
  <div className="status">
    <div className="status__icon">
      <StatusIcon />
    </div>
    <div className="status__detail">
      <h4>{title}</h4>
      <p>{message}</p>
    </div>
  </div>
);
StatusMessage.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  StatusIcon: PropTypes.func.isRequired,
};
export default StatusMessage;
