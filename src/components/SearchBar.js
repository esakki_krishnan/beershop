import React from 'react';
import { Card } from 'rmwc/Card';
import TextField, { TextFieldIcon } from 'rmwc/TextField';
import Select from 'rmwc/Select';

const SearchBar = ({ onEnter, onFilter, onSort, filterOption }) => (
  <div>
    <Card
      className="search-card"
      style={{
        width: '100%',
        display: 'flex',
        padding: '10px',
      }}
    >
      <TextField
        box
        label="Search..."
        withTrailingIcon={<TextFieldIcon use="search" />}
        onKeyPress={e => {
          if (e.key === 'Enter') onEnter(e.target.value);
        }}
      />
      <div>
        <Select
          box
          label="Sort By Alcohol Content"
          placeholder="Select Sort By"
          options={[
            { label: 'Low -> High', value: 0 },
            { label: 'High -> Low', value: 1 },
          ]}
          onChange={evt => onSort(evt.target.value)}
        />
        <Select
          box
          label="Filter By Style"
          placeholder="Select Style"
          options={filterOption}
          onChange={evt => onFilter(evt.target.value)}
        />
      </div>
    </Card>
  </div>
);

export default SearchBar;
