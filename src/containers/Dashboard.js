import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  LinearProgress,
  Card,
  Typography,
  ListDivider,
  Icon,
  Button,
} from 'rmwc';
import SearchBar from '../components/SearchBar';
import BeerList from '../components/BeerList';
import {
  getBeers,
  addToCart,
  updateFilterBy,
  updateSearchBy,
  updateSortBy,
} from '../actions/BeerAction';
import { BEERS_URL, DATA_STATUS } from '../constants/common';
import StatusMessage from '../components/StatusMessage';
import Spinner from '../components/Spinner';
import { getBeerStyles, getFilteredBeers } from '../selectors';

const mapStateToProps = state => ({
  beerStatus: state.beers.status,
  beers: getFilteredBeers(state),
  styles: getBeerStyles(state),
});
const mapDispatchToProps = dispatch => ({
  getBeers: bindActionCreators(getBeers, dispatch),
  addToCart: bindActionCreators(addToCart, dispatch),
  updateFilterBy: bindActionCreators(updateFilterBy, dispatch),
  updateSearchBy: bindActionCreators(updateSearchBy, dispatch),
  updateSortBy: bindActionCreators(updateSortBy, dispatch),
});
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      pageNo: 1,
      sortBy: null,
      filterBy: null,
      searchText: null,
    };
  }
  componentDidMount() {
    if (this.props.beerStatus === DATA_STATUS.NOT_LOADED)
      this.props.getBeers(BEERS_URL);
  }
  renderList() {
    const { beerStatus, addToCart, beers } = this.props;
    const { pageNo } = this.state;
    switch (beerStatus) {
      case DATA_STATUS.LOADED:
        return (
          <div style={{ marginTop: '20px' }}>
            <BeerList
              beerList={beers.slice(0, pageNo * 20)}
              onAddtoCart={addToCart}
            />
            <div style={{ width: '100%' }}>
              <Button
                style={{ display: 'block', margin: '20px auto' }}
                raised
                onClick={() =>
                  this.setState({
                    pageNo: this.state.pageNo + 1,
                  })
                }
              >
                LOADMORE
              </Button>
            </div>
          </div>
        );
      case DATA_STATUS.ERRORED:
        return (
          <StatusMessage
            message={beers.error || 'Unable to fetch data'}
            title="Error"
            StatusIcon={() => <Spinner />}
          />
        );
      default:
        return (
          <StatusMessage
            message="Loading beer data"
            title="Loading"
            StatusIcon={() => <Spinner />}
          />
        );
      // <Icon strategy="ligature" use="error_outline" />
      // return <LinearProgress determinate={false} />;
    }
  }
  render() {
    console.log(
      'Sort ',
      this.state.sortBy,
      this.state.filterBy,
      this.state.searchText,
    );
    const { updateFilterBy, updateSearchBy, updateSortBy } = this.props;
    return (
      <div>
        <SearchBar
          onEnter={t => updateSearchBy(t)}
          onSort={v => updateSortBy(v)}
          onFilter={v => updateFilterBy(v)}
          filterOption={this.props.styles}
        />
        {this.renderList()}
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
