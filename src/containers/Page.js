import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import AppBar from '../components/AppBar';

const Page = (MainComponent, layout, layoutProps) =>
  // eslint-disable-next-line react/prefer-stateless-function
  class extends Component {
    render() {
      const MyLayout = layout;
      return (
        <MyLayout {...layoutProps}>
          <MainComponent {...this.props} />
        </MyLayout>
      );
    }
  };

export default Page;

const BaseLayout = ({ headerTitle, children }) => (
  <div className="page">
    <AppBar
      headerTitle={headerTitle}
      menuIcon="local_bar"
      onMenuClick={() => {
        console.log('Clicked go Back');
      }}
    />
    <div className="content">{children}</div>
  </div>
);
BaseLayout.propTypes = {
  headerTitle: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

const BackLayoutComp = ({ headerTitle, children, history }) => (
  <div className="page">
    <AppBar
      headerTitle={headerTitle}
      menuIcon="arrow_back"
      onMenuClick={history.goBack}
    />
    <div className="content">{children}</div>
  </div>
);
BackLayoutComp.propTypes = {
  headerTitle: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  history: PropTypes.object,
};
BackLayoutComp.defaultProps = {
  history: {},
};
const BackLayout = withRouter(BackLayoutComp);

export { BaseLayout, BackLayout };
