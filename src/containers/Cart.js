import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getMyBeers } from '../selectors';
import StatusMessage from '../components/StatusMessage';
import Spinner from '../components/Spinner';
import BeerList from '../components/BeerList';

const mapStateToProps = state => ({
  beers: getMyBeers(state),
});

class Cart extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }
  renderList() {
    const { beers } = this.props;
    if (beers.length > 0) {
      return (
        <BeerList
          beerList={beers}
          onAddtoCart={() => {
            console.log('added');
          }}
        />
      );
    }
    return (
      <StatusMessage
        message="Loading beer data"
        title="Loading"
        StatusIcon={() => <Spinner />}
      />
    );
  }
  render() {
    return (
      <div>
        <div style={{ marginTop: '20px' }}>{this.renderList()}</div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Cart);
