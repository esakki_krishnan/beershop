import React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Page, { BaseLayout, BackLayout } from './Page';
import Dashboard from './Dashboard';
import Cart from './Cart';

const DashboardLayout = Page(Dashboard, BaseLayout, {
  headerTitle: 'BeerShop',
});
const CartLayout = Page(Cart, BackLayout, { headerTitle: 'My Beers' });

const App = () => (
  <BrowserRouter>
    <Route
      render={({ location }) => (
        <TransitionGroup>
          <CSSTransition
            key={location.key}
            classNames="pagefade"
            timeout={1000}
          >
            <Switch location={location}>
              <Route exact path="/" component={DashboardLayout} />
              <Route path="/cart" component={CartLayout} />
            </Switch>
          </CSSTransition>
        </TransitionGroup>
      )}
    />
  </BrowserRouter>
);
export default App;
