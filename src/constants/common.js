export const DATA_STATUS = {
  NOT_LOADED: 'NOT_LOADED',
  LOADING: 'LOADING',
  ERRORED: 'ERRORED',
  LOADED: 'LOADED',
};

export const BEERS_URL = 'http://starlord.hackerearth.com/beercraft';
