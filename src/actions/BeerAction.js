import * as idb from 'idb-keyval';
import * as types from '../constants/actionTypes';

const cacheFetch = url =>
  idb.get(url).then(localData => {
    if (localData) return localData;
    return fetch(url)
      .then(response => {
        if (!response.ok) {
          return Promise.reject(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(data => {
        idb.set(url, data);
        return data;
      });
  });
export function isErrored(error) {
  return {
    type: types.BEERS_ERRORED,
    payLoad: error,
  };
}

export function isLoading() {
  return {
    type: types.BEERS_LOADING,
  };
}

export function items(item) {
  return {
    type: types.BEERS_LOADED,
    payLoad: item,
  };
}

export function addToCart(item) {
  return {
    type: types.ADD_CART,
    payLoad: item,
  };
}

export function updateSearchBy(item) {
  return {
    type: types.UPDATE_SEARCHBY,
    payLoad: item,
  };
}

export function updateFilterBy(item) {
  return {
    type: types.UPDATE_FILTERBY,
    payLoad: item,
  };
}

export function updateSortBy(item) {
  return {
    type: types.UPDATE_SORTBY,
    payLoad: item,
  };
}

export function getBeers(url) {
  return dispatch => {
    dispatch(isLoading());
    cacheFetch(url)
      .then(item => dispatch(items(item)))
      .catch(error => {
        console.log(error);
        return dispatch(isErrored(error.toString()));
      });
  };
}
