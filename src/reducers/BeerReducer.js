import * as types from '../constants/actionTypes';
import * as Constants from '../constants/common';

const initialState = {
  status: Constants.DATA_STATUS.NOT_LOADED,
  data: [],
  error: null,
  sortBy: null,
  filterBy: null,
  searchText: null,
  pageNo: 1,
};
export default function(state = initialState, action) {
  switch (action.type) {
    case types.BEERS_LOADING:
      return Object.assign({}, state, {
        status: Constants.DATA_STATUS.LOADING,
      });
    case types.BEERS_LOADED:
      return Object.assign({}, state, {
        status: Constants.DATA_STATUS.LOADED,
        data: action.payLoad,
      });
    case types.BEERS_ERRORED:
      return Object.assign({}, state, {
        status: Constants.DATA_STATUS.ERRORED,
        error: action.payLoad,
      });
    case types.UPDATE_FILTERBY:
      return Object.assign({}, state, {
        filterBy: action.payLoad,
      });
    case types.UPDATE_SORTBY:
      return Object.assign({}, state, {
        sortBy: action.payLoad,
      });
    case types.UPDATE_SEARCHBY:
      return Object.assign({}, state, {
        searchText: action.payLoad,
      });
    default:
      return state;
  }
}
