import * as types from '../constants/actionTypes';

const initialState = {
  beerIds: [],
};
export default function(state = initialState, action) {
  switch (action.type) {
    case types.ADD_CART:
      return Object.assign({}, state, {
        beerIds: [].concat(state.beerIds, action.payLoad),
      });
    default:
      return state;
  }
}
