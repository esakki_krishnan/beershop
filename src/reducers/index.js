import { combineReducers } from 'redux';
import beerReducer from '../reducers/BeerReducer';
import cartReducer from '../reducers/CartReducer';

export default combineReducers({
  beers: beerReducer,
  cart: cartReducer,
});
