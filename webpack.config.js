// webpack.config.js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// plugins
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  inject: 'body',
});

const ExtractTextPluginConfig = new ExtractTextPlugin({
  filename: 'style.css',
  disable: false,
  allChunks: true,
});

const CopyAssetPluginConfig = new CopyWebpackPlugin([
  { from: './src/assets/fonts', to: './assets/fonts' },
]);

module.exports = {
  entry: {
    main: ['./src/index.js', './src/assets/styles/style.scss'],
    vendor: [
      'react',
      'react-dom',
      'redux',
      'react-redux',
      'redux-thunk',
      'react-router-dom',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { minimize: true } },
            {
              loader: 'sass-loader',
              options: {
                importer: url => {
                  if (url.indexOf('@material') === 0) {
                    const filePath = url.split('@material')[1];
                    const nodeModulePath = `./node_modules/@material/${filePath}`;
                    return { file: path.resolve(nodeModulePath) };
                  }
                  return { file: url };
                },
              },
            },
          ],
        }),
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    HtmlWebpackPluginConfig,
    ExtractTextPluginConfig,
    CopyAssetPluginConfig,
  ],
  target: 'web',
};
