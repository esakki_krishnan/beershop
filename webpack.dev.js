const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const HMRConfig = new webpack.HotModuleReplacementPlugin();

// plugins
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  inject: 'body',
});

const ExtractTextPluginConfig = new ExtractTextPlugin({
  filename: 'style.css',
  disable: true,
  allChunks: true,
});

const CopyAssetPluginConfig = new CopyWebpackPlugin([
  { from: './src/assets/fonts', to: './assets/fonts' },
]);

module.exports = {
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:9000',
    'webpack/hot/only-dev-server',
    './src/index.js',
    './src/assets/styles/style.scss',
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                importer: url => {
                  if (url.indexOf('@material') === 0) {
                    const filePath = url.split('@material')[1];
                    const nodeModulePath = `./node_modules/@material/${filePath}`;
                    return { file: path.resolve(nodeModulePath) };
                  }
                  return { file: url };
                },
              },
            },
          ],
        }),
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    HtmlWebpackPluginConfig,
    ExtractTextPluginConfig,
    HMRConfig,
    CopyAssetPluginConfig,
  ],
  target: 'web',
  devtool: 'cheap-source-map',
  devServer: {
    historyApiFallback: true,
    hot: true,
    contentBase: path.join(__dirname, 'build'),
    publicPath: '/',
    port: 9000,
  },
};
